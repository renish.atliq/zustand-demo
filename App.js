import React, {useState} from 'react';
import {
  Button,
  FlatList,
  Image,
  Pressable,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';
import useStore from './App/Store';

const App = () => {
  const {taskList, addTask, deleteTask, updateTask} = useStore();
  const [task, setTask] = useState('');
  const [itemToDelete, setItemToDelete] = useState(null);

  const saveTask = task => {
    setTask(task);
  };

  const addTaskToList = () => {
    if (task.trim().length > 0) {
      addTask(task);
      setTask('');
    } else {
      setTask('');
      alert('Please enter a task');
    }
  };

  const deleteTaskFromList = id => {
    deleteTask(id);
  };

  const editTaskFromList = (id, name) => {
    setItemToDelete(id);
    setTask(name);
  };

  const updateTaskToList = () => {
    if (task.trim().length > 0) {
      updateTask(itemToDelete, task);
      setItemToDelete(null);
      setTask('');
    } else {
      setTask('');
      alert('Please enter a task');
    }
  };

  return (
    <SafeAreaView>
      <StatusBar />
      <View>
        <FlatList
          data={taskList}
          renderItem={({item, index}) => (
            <View style={styles.rowCenter}>
              <Text>
                {index + 1}. {item.name}
              </Text>
              <View style={styles.rowCenter}>
                <Pressable onPress={() => editTaskFromList(item.id, item.name)}>
                  <Image
                    source={{
                      uri: 'https://img.icons8.com/color/48/000000/edit.png',
                    }}
                    style={{width: 20, height: 20, tintColor: 'black'}}
                  />
                </Pressable>
                <Pressable onPress={() => deleteTaskFromList(item.id)}>
                  <Image
                    source={{
                      uri: 'https://cdn-icons-png.flaticon.com/512/1214/1214428.png',
                    }}
                    style={{width: 20, height: 20, tintColor: 'black'}}
                  />
                </Pressable>
              </View>
            </View>
          )}
          ListEmptyComponent={() => (
            <View style={styles.center}>
              <Text>No tasks</Text>
            </View>
          )}
        />

        <TextInput
          style={{
            height: 40,
            borderColor: 'gray',
            borderWidth: 1,
          }}
          value={task}
          onChangeText={text => saveTask(text)}
        />
        <View style={styles.rowCenter}>
          <Button title="Add as New" onPress={addTaskToList} />
          <Button title="Update" onPress={updateTaskToList} />
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  listEmptyView: {
    flex: 1,
  },

  center: {
    alignItems: 'center',
  },

  rowCenter: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});

export default App;
