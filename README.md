Zustand, an alternative to redux, also has support to persist the data just like in redux.

A small, fast and scalable bearbones state-management solution using simplified flux principles. Has a comfy api based on hooks, isn't boilerplatey or opinionated.

Don't disregard it because it's cute. It has quite the claws, lots of time was spent to deal with common pitfalls, like the dreaded zombie child problem, react concurrency, and context loss between mixed renderers. It may be the one state-manager in the React space that gets all of these right.

App/store.js file contains the basic store created which is used to store state there as a centralized store and get a single truth of the existing value of state, if any state state needs to persist there is also a commented code in the same store.js file just for the reference.

Happy coding :-)
