import create from 'zustand';
import {persist} from 'zustand/middleware';
import AsyncStorage from '@react-native-async-storage/async-storage';

const useStore = create(set => ({
  taskList: [],
  addTask: task =>
    set(state => ({
      taskList: [
        ...state.taskList,
        {
          name: task,
          id: Math.floor(Math.random() * 10000),
          time: new Date(),
        },
      ],
    })),
  deleteTask: id =>
    set(state => ({
      taskList: state.taskList.filter(task => task.id !== id),
    })),
  updateTask: (id, name) =>
    set(state => ({
      taskList: state.taskList.map(task =>
        task.id === id ? {...task, name} : task,
      ),
    })),
}));

// Persist the state to AsyncStorage
// const useStore = create(
//   persist((set) => ({
// Add the state and actions to the store here.
//   })),
// );
export default useStore;
